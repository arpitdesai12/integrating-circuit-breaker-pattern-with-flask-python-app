from flask import Flask, jsonify, abort, request
from flask_sqlalchemy import SQLAlchemy
import datetime
import json
import redis
import os

app = Flask(__name__)
app.config['SQLALCHEMY_DATABASE_URI'] = 'mysql+pymysql://root:Arpeet123@localhost/expenses'
db = SQLAlchemy(app)

class Expenses(db.Model):
        __tablename__ = 'Exp'
        id = db.Column(db.Integer, primary_key=True)
        name = db.Column(db.String(20))
        email = db.Column(db.String(30))
        category = db.Column(db.String(100))
        description = db.Column(db.String(20))
        link = db.Column(db.String(100))
        estimated_costs = db.Column(db.String(30))
        submit_date = db.Column(db.String(10))
        status = db.Column(db.String(10))
        decision_date = db.Column(db.String(10))

        def __init__(self, name, email, category,description, link, estimated_costs, submit_date,status,decision_date):
            self.name = name
            self.email = email
            self.category = category
            self.description = description
            self.link = link
            self.estimated_costs = estimated_costs
            self.submit_date = submit_date 
                                
@app.route('/v1/expenses/<int:id>', methods = ['GET'])
def get_exp(id):

        exp = Expenses.query.get(id)
        if exp == None:
            return json.dumps({}), 404
        else:     
            exp_new = {'id': exp.id, 
                    'name': exp.name,
                    'email': exp.email,
                    'category': exp.category,
                    'description': exp.description,
                    'link': exp.link,
                    'estimated_costs': exp.estimated_costs,
                    'submit_date': exp.submit_date,
                    'status': exp.status,
                    'decision_date': exp.decision_date}
            return json.dumps(exp_new)       


@app.route('/v1/expenses/<int:id>', methods = ['POST'])
def create_exp(id):
        
        exp = Expenses('name','email', 'category','description', 'link', 'estimated_costs', 'submit_date','status','decision_date')
        data = json.loads(request.data)
        exp.id=id
        exp.name=data['name']
        exp.email=data['email']
        exp.category=data['category']
        exp.description=data['description']
        exp.link=data['link']
        exp.estimated_costs=data['estimated_costs']
        exp.submit_date=data['submit_date']
        exp.status='pending'
        exp.decision_date=""
        db.session.add(exp)
        db.session.commit()
        exp_new = {'id': exp.id, 
                 'name': exp.name,
                 'email': exp.email,
                 'category': exp.category,
                 'description': exp.description,
                 'link': exp.link,
                 'estimated_costs': exp.estimated_costs,
                 'submit_date': exp.submit_date,
                 'status': exp.status,
                 'decision_date': exp.decision_date}
        return json.dumps(exp_new), 201

if __name__ == "__main__":
    db.create_all()
    r = redis.StrictRedis(host="127.0.0.1", port=6379, db=0)
    ports = r.get('portentries')
    if ports:
        ports = ports.decode('utf-8').split(',')
    else:
        ports = []
    if '5002' not in ports:
        ports.append('5002')
    r.set('portentries', ','.join(ports))
    app.run(host="0.0.0.0", port=5002, debug=True)
