import select
import socket
import sys
import time
import redis
import circuit_breaker 

buffer_size = 4096
delay = 0.0001
forward_to = '127.0.0.1'

class Forward:
    def __init__(self):
        self.forward = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    @circuit_breaker.CircuitBreaker(max_failure_to_open=3, reset_timeout=10)
    def start(self, host, port):
        try:
            self.forward.connect((host, port))
            return self.forward
        except Exception as e:
            e.port = port
            raise e
       
class TheServer:
    input_list = []
    channel = {}

    def AddPorts(self):
        print('AddPorts')
        self.ports = self.r.get('portentries')
        if self.ports:
            self.ports = self.ports.decode('utf-8').split(',')
        else:
            self.ports = []
        print(self.ports)
        self.Port_no = []
        for port in self.ports:
            self.Port_no.append(int(port))
        print(self.Port_no)

    def RemovePorts(self, port):
        print('RemovePorts')
        self.ports.remove(str(port))
        self.r.set('portentries', ','.join(self.ports))
        self.AddPorts()

    def __init__(self, host, port):
        self.r = redis.StrictRedis(host="localhost", port=6379, db=0)
        self.server = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.server.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        self.server.bind((host, port))
        self.server.listen(200)
        self.AddPorts()

    def main_loop(self):
        self.input_list.append(self.server)
        while 1:
            time.sleep(delay)
            ss = select.select
            inputready, outputready, exceptready = ss(self.input_list, [], [])
            for self.s in inputready:
                if self.s == self.server:
                    self.on_accept()
                    break
                self.data = self.s.recv(buffer_size)
                if len(self.data) == 0:
                    self.on_close()
                    break
                else:
                    self.on_recv()

    def on_accept(self):
        retry = True
        while retry:
            newport = self.Port_no[0]
            print self.Port_no[0]
            self.Port_no = self.Port_no[1:]
            print self.Port_no[0]
            self.Port_no.append(newport)
            retry = False
            try:
                forward = Forward().start(host = forward_to, port = newport)
                clientsock, clientaddr = self.server.accept()
                if forward:
                    print(clientaddr, "has connected")
                    self.input_list.append(clientsock)
                    self.input_list.append(forward)
                    self.channel[clientsock] = forward
                    self.channel[forward] = clientsock
                else:
                    print("Can't establish connection with remote server.")
                    print("Closing connection with client side", clientaddr)
                    clientsock.close()
            except Exception as e:
                print(e.args)
                if e.args[0].startswith("Port Failure"):
                    self.RemovePorts(newport)
                    retry = True

    def on_close(self):
        print(self.s.getpeername(), "has disconnected")
        self.input_list.remove(self.s)
        self.input_list.remove(self.channel[self.s])
        out = self.channel[self.s]
        self.channel[out].close()  
        self.channel[self.s].close()
        del self.channel[out]
        del self.channel[self.s]

    def on_recv(self):
        data = self.data
        print(data)
        self.channel[self.s].send(data)

if __name__ == '__main__':
    server = TheServer('', 9090)
    try:
        server.main_loop()
    except KeyboardInterrupt:
        print("Ctrl C - Stopping server")
        sys.exit(1)
